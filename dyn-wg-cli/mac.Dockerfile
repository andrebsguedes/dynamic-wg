# syntax = docker/dockerfile:1.0-experimental

FROM multiarch/crossbuild:latest AS build

ENV PATH "/root/.cargo/bin:$PATH"

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y \
  && echo $PATH \
  && rustup target add x86_64-apple-darwin

WORKDIR /workdir

COPY credentials.json mac-compiler.sh Cargo.toml /workdir/
COPY src /workdir/src
COPY cargoconfig /workdir/.cargo/config.toml

ENV TARGET_CC /workdir/mac-compiler.sh

RUN --mount=type=cache,target=/workdir/target \
    --mount=type=cache,target=/usr/local/cargo/git \
    --mount=type=cache,target=/usr/local/cargo/registry \
    ["cargo", "build", "--release", "--target", "x86_64-apple-darwin"]

RUN --mount=type=cache,target=/workdir/target \
    ["cp", "/workdir/target/x86_64-apple-darwin/release/dyn-wg-cli", "/workdir/entry"]

FROM scratch

WORKDIR /dist

COPY --from=build /workdir/entry /dist/entry

CMD ["/dist/entry"]
