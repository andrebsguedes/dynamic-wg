type Exception = Box<dyn std::error::Error + Send + Sync + 'static>;

use oauth2::{
    CodeTokenRequest,
    AuthorizationCode,
    AuthUrl,
    ClientId,
    ClientSecret,
    CsrfToken,
    PkceCodeChallenge,
    PkceCodeVerifier,
    RedirectUrl,
    Scope,
    TokenResponse,
    TokenUrl
};

use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use url::Url;
use failure::Error;
use hyper::{Body, Error as HyperError, Request, Response, Server, Method, StatusCode};
use hyper::service::{make_service_fn, service_fn};
use std::sync::Arc;
use futures_util::stream::StreamExt;
use serde::{Serialize, Deserialize};
use tokio::process::Command;
use std::fs::{self, File, OpenOptions};
use std::io::{self, Read, Write, Seek, SeekFrom};
use std::path::Path;
use std::process::{Stdio};
use once_cell::sync::Lazy;

static CREDENTIALS_STR: &str = include_str!("../credentials.json");

static CREDENTIALS: Lazy<Credentials> = Lazy::new(|| {
    serde_json::from_str(CREDENTIALS_STR).expect("failed to parse stored credentials")
});

#[derive(Debug, Deserialize, Serialize)]
pub struct Credentials {
    api_connect_url: String,
    client_secret: String,
    client_id: String,
    auth_url: String,
    token_url: String,
    scopes: Vec<String>
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Params {
    code: String,
    state: String
}

#[derive(Debug, Deserialize, Serialize)]
pub struct AllowedIp {
    ip: String,
    cidr: u16
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Peer {
    public_key: String,
    endpoint: String,
    allowed_ips: Vec<AllowedIp>
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ApiRequest {
    public_key: String
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ApiResponse {
    address: String,
    peers: Vec<Peer>,
    mtu: Option<u64>
}

pub fn serialize_peer (peer: Peer) -> String {
    let decoded_public_key = hex::decode(peer.public_key.as_bytes()).unwrap();
    let base64_public_key = base64::encode(&decoded_public_key);
    let allowed_ips_len = peer.allowed_ips.len();
    let allowed_ips: String = peer.allowed_ips.into_iter().map(|addr| format!("{}/{}", addr.ip, addr.cidr)).collect::<Vec<String>>().join(",");
    let allowed_ips_string = if allowed_ips_len > 0 {
        format!("AllowedIPs = {}", allowed_ips)
    } else {
        "".to_string()
    };

    format!(r#"[Peer]
PublicKey = {public_key}
{allowed_ips}
Endpoint = {endpoint}
PersistentKeepalive = 25"#, public_key=base64_public_key, allowed_ips=allowed_ips_string, endpoint=peer.endpoint)
}

pub fn serialize_wg_quick_config (pk: String, response: ApiResponse) -> String {
    let peers_string = response.peers.into_iter().map(|peer| serialize_peer(peer)).collect::<Vec<String>>().join("\n\n");
    let mtu_string = response.mtu.unwrap_or(1380);
    let interface_string = format!(r#"[Interface]
Address = {addr}/24
ListenPort = 32701
MTU = {mtu}
PrivateKey = {pk}

{peers}
"#, addr=response.address, peers=peers_string, pk=pk, mtu=mtu_string);
    interface_string
}

pub async fn setup_keys () -> io::Result<(String, String)> {
    let home = std::env::var("HOME").expect("Missing HOME env var");
    let home_path = Path::new(&home);
    let ssh_dir_path = home_path.join(".ssh");
    fs::create_dir_all(&ssh_dir_path)?;
    let pk_path = ssh_dir_path.join("dynwgpk");
    let mut file = match File::open(&pk_path) {
        Ok(file) => file,
        Err(err) => {
            if err.kind() == io::ErrorKind::NotFound {
                let mut file = OpenOptions::new().write(true).truncate(true).read(true).create(true).open(&pk_path)?;
                let gen_key = Command::new("wg").arg("genkey").output();
                let mut out = gen_key.await?;
                assert!(out.status.success());
                out.stdout.pop();
                file.write_all(&out.stdout)?;
                file.seek(SeekFrom::Start(0))?;
                file
            } else {
                return Err(err);
            }
        }
    };
    let mut pk_contents = String::new();
    file.read_to_string(&mut pk_contents)?;
    file.seek(SeekFrom::Start(0))?;
    let pub_key_cmd = Command::new("wg")
        .arg("pubkey")
        .stdin(file)
        .output();
    let mut out = pub_key_cmd.await?;
    assert!(out.status.success());
    out.stdout.pop();
    Ok((pk_contents, String::from_utf8(out.stdout).unwrap()))
}

pub async fn handle_response (pk: String, response: ApiResponse) -> io::Result<()> {
    let home = std::env::var("HOME").expect("Missing HOME env var");
    let home_path = Path::new(&home);
    let ssh_dir_path = home_path.join(".ssh");
    fs::create_dir_all(&ssh_dir_path)?;
    let conf_path = ssh_dir_path.join("dynwg.conf");

    let config = serialize_wg_quick_config(pk, response);

    let mut file = OpenOptions::new().write(true).truncate(true).create(true).open(&conf_path)?;
    file.write_all(config.as_bytes())?;

    let wg_quick_down_cmd = Command::new("wg-quick")
        .arg("down")
        .arg(conf_path.to_str().unwrap())
        .output();
    let mut out = wg_quick_down_cmd.await?;

    let wg_quick_cmd = Command::new("wg-quick")
        .arg("up")
        .arg(conf_path.to_str().unwrap())
        .stderr(Stdio::inherit())
        .output();
    let mut out = wg_quick_cmd.await?;

    println!("{}", String::from_utf8_lossy(out.stderr.as_ref()));

    if out.status.success() {
        Ok(())
    } else {
        Err(io::Error::new(io::ErrorKind::Other, "Failed to bring interface up"))
    }
}

async fn router_service (req: Request<Body>, client: Arc<BasicClient>, verifier_string: String, csrf_token: CsrfToken) -> Result<Response<Body>, HyperError> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/auth") => {
            let query = req.uri().query().unwrap();
            let params: Params = serde_qs::from_str(query).unwrap();
            if (params.state == *csrf_token.secret()) {
                let verifier = PkceCodeVerifier::new(verifier_string);
                let token_result = client
                    .exchange_code(AuthorizationCode::new(params.code))
                    // Set the PKCE code verifier.
                    .set_pkce_verifier(verifier)
                    .request_async(async_http_client)
                    .await;
                if let Ok(token) = token_result {
                    let (private_key, public_key) = setup_keys().await.unwrap();
                    let decoded_public_key = base64::decode(public_key.as_bytes()).unwrap();
                    let hex_public_key = hex::encode(&decoded_public_key);

                    let request = ApiRequest {
                        public_key: hex_public_key
                    };
                    let req_client = reqwest::Client::new();
                    let response: ApiResponse = req_client.post(&CREDENTIALS.api_connect_url)
                        .bearer_auth(token.access_token().secret())
                        .json(&request)
                        .send()
                        .await.unwrap()
                        .json()
                        .await.unwrap();

                    let response = match handle_response(private_key, response).await {
                        Ok(_) => {
                            Response::builder()
                                .status(StatusCode::OK)
                                .body(Body::from("{ \"success\": true }"))
                                .unwrap()
                        }
                        Err(err) => {
                            Response::builder()
                                .status(StatusCode::INTERNAL_SERVER_ERROR)
                                .body(Body::from(format!("{{ \"error\": \"{}\" }}", err)))
                                .unwrap()
                        }
                    };

                    tokio::spawn(async {
                        tokio::time::delay_for(std::time::Duration::from_millis(300)).await;
                        println!("[!] Login successful");
                        std::process::exit(0);
                    });

                    Ok(response)
                } else {
                    println!("{:?}", token_result);
                    let response = Response::builder()
                        .status(StatusCode::BAD_REQUEST)
                        .body(Body::from("Failed to authenticate"))
                        .unwrap();
                    Ok(response)
                }
            } else {
                let response = Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body(Body::from("Invalid STATE"))
                    .unwrap();
                Ok(response)
            }
        }
        _ => {
            let response = Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Body::from("404"))
                .unwrap();
            Ok(response)
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tokio::spawn(async {
        tokio::signal::ctrl_c().await.unwrap();
        println!("Shutdown");
        std::process::exit(0);
    });
    let client = Arc::new(BasicClient::new(
        ClientId::new(CREDENTIALS.client_id.clone()),
        Some(ClientSecret::new(CREDENTIALS.client_secret.clone())),
        AuthUrl::new(CREDENTIALS.auth_url.clone())?,
        Some(TokenUrl::new(CREDENTIALS.token_url.clone())?)
    ).set_redirect_url(RedirectUrl::new("http://127.0.0.1:7000/auth".to_string())?));

    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

    let mut builder = client.authorize_url(CsrfToken::new_random);

    for scope in CREDENTIALS.scopes.clone() {
        builder = builder.add_scope(Scope::new(scope));
    }

    let (auth_url, csrf_token) = builder
        // Set the PKCE code challenge.
        .set_pkce_challenge(pkce_challenge)
        .url();

    println!("Browsing to: {}", auth_url);

    let verifier_string = pkce_verifier.secret().to_owned();

    let addr = ([0, 0, 0, 0], 7000).into();
    let make_service = make_service_fn(move |_| {
        let client = client.clone();
        let verifier_string = verifier_string.clone();
        let csrf_token = csrf_token.clone();
        async {
            Ok::<_, HyperError>(service_fn(move |req| {
                router_service(req, client.clone(), verifier_string.clone(), csrf_token.clone())
            }))
        }
    });
    let server = Server::bind(&addr).serve(make_service);
    if webbrowser::open(auth_url.as_str()).is_ok() {
    }
    server.await.unwrap();

    Ok(())
}
