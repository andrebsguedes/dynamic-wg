DOCKER_BUILDKIT=1 docker build -t dyn-wg-cli:latest .
docker rm dyn-wg || true
docker create --name dyn-wg -t dyn-wg-cli:latest
mkdir -p ./dist/linux/dynwg
docker cp dyn-wg:/app/entry ./dist/linux/dynwg/dynwg
docker rm dyn-wg
