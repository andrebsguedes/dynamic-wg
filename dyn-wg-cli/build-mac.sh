DOCKER_BUILDKIT=1 docker build -f mac.Dockerfile -t dyn-wg-cli-mac:latest .
docker rm dyn-wg-mac || true
docker create --name dyn-wg-mac -t dyn-wg-cli-mac:latest
mkdir -p ./dist/darwin/dynwg
docker cp dyn-wg-mac:/dist/entry ./dist/darwin/dynwg/dynwg
docker rm dyn-wg-mac
