#!/bin/bash
export CROSS_TRIPLE="x86_64-apple-darwin"

exec crossbuild cc -mmacosx-version-min=10.7 "$@"
