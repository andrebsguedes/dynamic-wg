
const routes = [
  {
    path: '/',
    component: () => import('layouts/SimpleLayout.vue'),
    children: [
      {
        path: '/users',
        component: () => import('pages/Users.vue'),
        props: route => ({ email: route.query.email })
      },
      {
        path: '/roles',
        component: () => import('pages/Roles.vue'),
        props: route => ({ name: route.query.name })
      },
      {
        path: '/sites',
        component: () => import('pages/Sites.vue'),
        props: route => ({ id: route.query.id })
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
