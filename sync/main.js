// ESM syntax is supported.
import { Etcd3 } from 'etcd3'
import got from 'got'
import fs from 'fs'

const ETCD_CLIENT_CERT_FILE = process.env.ETCD_CLIENT_CERT_FILE
const ETCD_CLIENT_KEY_FILE = process.env.ETCD_CLIENT_KEY_FILE
const ETCD_CA_CERT_FILE = process.env.ETCD_CA_CERT_FILE

var client = newClient()

function newClient () {
  return new Etcd3({
    hosts: [process.env.ETCD_HOST || `127.0.0.1:2379`],
    auth: process.env.ETCD_USER && {
      username: process.env.ETCD_USER,
      password: process.env.ETCD_PASS
    },
    credentials: ETCD_CA_CERT_FILE && {
      certChain: ETCD_CLIENT_CERT_FILE && fs.readFileSync(ETCD_CLIENT_CERT_FILE),
      privateKey: ETCD_CLIENT_KEY_FILE && fs.readFileSync(ETCD_CLIENT_KEY_FILE),
      rootCertificate: ETCD_CA_CERT_FILE && fs.readFileSync(ETCD_CA_CERT_FILE)
    }
  })
}

let ifaceUid = process.argv[2]
let ifaceIp = process.env.DYN_WG_ADDR || `127.0.0.1`

if (!ifaceUid) {
  console.log(`Missing ifaceUid`)
  process.exit(1)
}

function filterRules (peer) {
  if (peer) {
    if (peer.rules && Array.isArray(peer.rules)) {
      let rules = []

      for (var rule of peer.rules) {
        if (rule.only && rule.only.length && rule.only.indexOf(ifaceUid) === -1) {
          continue
        }
        if (rule.except && rule.except.indexOf(ifaceUid) !== -1) {
          continue
        }

        rules.push(rule)
      }

      peer.rules = rules
    }
  }

  return peer
}

// Cancellable Timer
class Timer {
  constructor (fn, time) {
    this.id = 0
    this.fn = fn
    this.time = time
    this.start()
  }

  start () {
    let localId
    localId = setTimeout(async () => {
      try {
        console.log(localId)
        if (localId === this.id) {
          await this.fn()
        }
      } catch (err) {
        console.log('Timer failed with:', err)
      }
    }, this.time)

    this.id = localId
  }

  clear () {
    clearTimeout(this.id)
  }

  reset () {
    this.clear()
    this.start()
  }
}

let globalPeerPrefix = 'dyn-wg:global:peers'
var lastRevision = null

async function start () {
  let ifaceData = await client.get(`dyn-wg:sites:${ifaceUid}`).json()

  if (!ifaceData) {
    throw new Error(`Missing iface data for ${ifaceUid}`)
  }

  let ifaceConfig = {
    address: ifaceData.address,
    listen_port: ifaceData.listen_port,
    private_key: ifaceData.private_key
  }

  let builder = client.watch()
    .prefix(globalPeerPrefix)
    .withPreviousKV()

  if (lastRevision !== null) {
    builder = builder.startRevision(lastRevision)
  }

  let peerWatcher = await builder
    .create()

  console.log(`Starting`, Date.now())

  let timer = new Timer(async () => {
    console.log(`Timeout while waiting for events, restarting all`)

    let cancel = (async () => {
      try {
        peerWatcher.removeAllListeners()
        await peerWatcher.cancel()
      } catch (err) {
        console.log(`Failed to cancel watcher`, err)
      }
    })

    cancel()
    client.close()

    client = newClient()

    try {
      await start ()
      console.log(`Restart complete`)
    } catch (err) {
      console.log(`Failed to restart, fatal, exiting`, err)
      process.exit(1)
    }
  }, 20 * 60 * 1000)

  peerWatcher.on('connected', () => {
    console.log(`Reconnected to ETCD`)
    timer.reset()
  })

  peerWatcher.on('disconnected', () => {
    console.log(`Disconnected from ETCD`)
  })

  peerWatcher.on('put', async (res, prev) => {
    if (res.key.toString() !== `${globalPeerPrefix}:${ifaceData.public_key}`) {
      console.log(`got put ${res.value.toString()}`)
      try {
        await got.put(`http://${ifaceIp}:3000/peer`, {
          json: true,
          body: filterRules(JSON.parse(res.value.toString()))
        })
      } catch (err) {
        console.log(err)
      }
    }
  })

  peerWatcher.on('data', async (res) => {
    try {
      console.log('Got data, reseting timer', Date.now())
      timer.reset()
      lastRevision = res.header.revision
    } catch (err) {
      console.log(`Data callback failed`, err)
      process.exit(1)
    }
  })

  peerWatcher.on('delete', async (res, prev) => {
    if (res.key.toString() !== `${globalPeerPrefix}:${ifaceData.public_key}`) {
      let public_key = res.key.toString().replace(`${globalPeerPrefix}:`, '')
      console.log(`got delete ${public_key}`)
      try {
        await got.delete(`http://${ifaceIp}:3000/peer`, {
          json: true,
          body: (prev && prev.value && filterRules(JSON.parse(prev.value.toString()))) || {
            public_key,
            allowed_ips: []
          }
        })
      } catch (err) {
        console.log(err)
      }
    }
  })

  peerWatcher.on('error', async (err) => {
    console.log(`Watcher error, fatal, shutting down`)
    console.log(err)
    process.exit(1)
  })

  if (lastRevision === null) {
    let peersResponse = await client.getAll().prefix(globalPeerPrefix).exec()
    lastRevision = peersResponse.header.revision

    let peers = peersResponse.kvs
      .filter(kv => kv.key.toString() !== `${globalPeerPrefix}:${ifaceData.public_key}`)
      .map(kv => filterRules(JSON.parse(kv.value.toString())))

    let res = await got.post(`http://${ifaceIp}:3000/interface`, {
      json: true,
      body: {
        ...ifaceConfig,
        peers
      }
    })
    console.log(ifaceConfig)
    console.log(res.body)
    console.log(peers)
  } else {
    console.log(`Reseting from revision ${lastRevision}`)
  }

}

async function main () {
  await start()
}

if (!module.parent) {
  main()
  .catch(err => {
    client.close()
    console.error(err)
    process.exit(1)
  })
  .then(console.log)
}

export {}
