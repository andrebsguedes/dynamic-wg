// ESM syntax is supported.
import { Etcd3 } from 'etcd3'

const client = new Etcd3({
  hosts: [process.env.ETCD_HOST || `127.0.0.1:2379`],
  auth: {
    username: process.env.ETCD_USER,
    password: process.env.ETCD_PASS
  }
})

let ifaceUidA = 'dukto-wg'
let ifaceUidB = 'local-wg'
let ifaceUidC = 'local-cluster-wg2'

let globalPeerPrefix = 'dyn-wg:global:peers'

let clear = !!process.argv[2]

async function main () {

  let ifaceConfigA = {
    address: '10.1.1.20/24',
    listen_port: 32701,
    private_key: '1847aedcf8a204bb3b128d989aa9d4228abeb7f7e331dca64b0d7908b7983aa5',
    public_key: '0cc692b72f47b01f9042fbe66904d1642c115f1ee1c3a32aa1c39d0412b96c13'
  }

  let ifaceConfigB = {
    address: '10.1.1.21/24',
    listen_port: 59050,
    private_key: '2d8e95696caa89972750029ba24bfd4c0bc1f9b608c064d9dff8cfb56ce46241',
    public_key: 'a0af56a1a02c87e23231a0730c6999342262629174dc118a9b1490be3c899c22'
  }

  let ifaceConfigC = {
    address: '10.1.1.24/24',
    listen_port: 32701,
    private_key: '818ddcb25dd4dd10170d141be531ee21318da3a0385905bc97944d6c7da4448f',
    public_key: 'a573193e0b0d95175579aff17816ce88e3e38821cf8af7a0fa41660cd355ad32'
  }

  await client.put(`dyn-wg:${ifaceUidA}`).value(JSON.stringify(ifaceConfigA))
  await client.put(`dyn-wg:${ifaceUidB}`).value(JSON.stringify(ifaceConfigB))
  await client.put(`dyn-wg:${ifaceUidC}`).value(JSON.stringify(ifaceConfigC))

  let peerA = {
    public_key: '0cc692b72f47b01f9042fbe66904d1642c115f1ee1c3a32aa1c39d0412b96c13',
    endpoint: '172.17.0.3:53050',
    allowed_ips: [{ ip: '10.1.1.20', cidr: 32 }]
  }

  let peerB = {
    public_key: 'a0af56a1a02c87e23231a0730c6999342262629174dc118a9b1490be3c899c22',
    endpoint: '172.17.0.4:53050',
    allowed_ips: [{ ip: '10.1.1.21', cidr: 32 }]
  }

  let peerC = {
    public_key: 'a573193e0b0d95175579aff17816ce88e3e38821cf8af7a0fa41660cd355ad32',
    allowed_ips: [{ ip: '10.1.1.24', cidr: 32 }],
    persistent_keepalive_interval: 25
  }

  let allPeers = [peerA, peerB, peerC]

  if (clear) {
    for (let peer of allPeers) {
      await client.delete().key(`${globalPeerPrefix}:${peer.public_key}`)
    }
  } else {
    let peers = [peerA, peerB, peerC]

    for (let peer of peers) {
      await client.put(`${globalPeerPrefix}:${peer.public_key}`)
        .value(JSON.stringify(peer))
    }
  }
}

if (!module.parent) {
  main()
  .catch(err => {
    client.close()
    console.error(err)
    process.exit(1)
  })
  .then(console.log)
}

export {}
