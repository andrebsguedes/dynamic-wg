// ESM syntax is supported.
import got from 'got'
import express from 'express'
import bodyParser from 'body-parser'
import { promisify } from 'util'
import cookieSession from 'cookie-session'

import {
  SESSION_SECRET,
  isGoogle,
  isGeneric
} from './config'

import {
  generateURLGoogle,
  generateURLGeneric,
  requestEmailGoogle,
  requestEmailGeneric,
  getConnectProfile
} from './providers'

import { client } from './db'

export class ApiError extends Error {
  constructor (code, message) {
    super(message)
    this.code = code
    this.message = message
    Error.captureStackTrace(this, ApiError)
  }
}

async function resolveUserRules (client, user) {
  if (user) {
    let roles = user.roles || []
    let userRules = user.rules || []

    let allRules = []

    for (var role of roles) {
      let roleData = await client.get(`${rolesPrefix}:${role}`).json()
      if (roleData && Array.isArray(roleData.rules)) {
        allRules = allRules.concat(roleData.rules)
      }
    }

    allRules = allRules.concat(userRules)

    return allRules
  } else {
    return []
  }
}

function routesForSite (siteId, rules) {
  let ranges = []

  for (var rule of rules) {
    if (rule.only && rule.only.length && rule.only.indexOf(siteId) === -1) {
      continue
    }
    if (rule.except && rule.except.indexOf(siteId) !== -1) {
      continue
    }

    if (!rule.direction || rule.direction === 'INGRESS') {
      if (!rule.action || rule.action === 'ALLOW') {
        for (var target of rule.targets) {
          let [ip, cidr] = target.split('/')
          cidr = Number(cidr) || 32
          ranges.push({ ip, cidr })
        }
      }
    }
  }

  return ranges
}

const usersPrefix = 'dyn-wg:global:users'
const userPublicKeyPrefix = 'dyn-wg:global:user-public-key'
const peersPrefix = 'dyn-wg:global:peers'
const sitesPrefix = 'dyn-wg:sites'
const rolesPrefix = 'dyn-wg:roles'
const roleUsersPrefix = 'dyn-wg:role-users'

async function replacePeer (client, id, peerConfig) {
    let lastPubkey = await client.get(`${userPublicKeyPrefix}:${id}`).json()

    if (lastPubkey) {
      if (lastPubkey !== peerConfig.public_key) {
        console.log(`Replacing pubkey`)
        await Promise.all([
          client.delete().key(`${peersPrefix}:${lastPubkey}`),
          client.put(`${userPublicKeyPrefix}:${id}`).value(JSON.stringify(peerConfig.public_key)),
          client.put(`${peersPrefix}:${peerConfig.public_key}`).value(JSON.stringify(peerConfig))
        ])
      } else {
        console.log(`Refreshing same pubkey`)
        await Promise.all([
          client.put(`${userPublicKeyPrefix}:${id}`).value(JSON.stringify(peerConfig.public_key)),
          client.put(`${peersPrefix}:${peerConfig.public_key}`).value(JSON.stringify(peerConfig))
        ])
      }
    } else {
      await Promise.all([
        client.put(`${userPublicKeyPrefix}:${id}`).value(JSON.stringify(peerConfig.public_key)),
        client.put(`${peersPrefix}:${peerConfig.public_key}`).value(JSON.stringify(peerConfig))
      ])
    }
}

async function replacePeerTxn (id, peerConfig) {
  await client.stm({ isolation: 2 }).transact(async (tx) => {
    await replacePeer(tx, id, peerConfig)
  })
}


const app = express()
app.use(cookieSession({
  name: 'session',
  secret: SESSION_SECRET,
  maxAge: 30 * 24 * 60 * 60 * 1000 /* 1 month */
}))
app.use(bodyParser.json())

app.post('/connect', async (req, res) => {
  const authorization = req.headers['authorization']
  if (authorization) {
    const [authType, token] = authorization.trim().split(' ')
    if (authType === 'Bearer') {
      try {
        let public_key = req.body.public_key
        if (!public_key) {
          return res.status(400).json({ error: 'Missing public_key' })
        } else {
          console.log(`got public key`, public_key)
        }

        const response = await getConnectProfile(token)

        if (response.email) {
          let user = await client.get(`${usersPrefix}:${response.email}`).json()
          if (user) {
            console.log(`Logging user`, response.email)
            let rules = await resolveUserRules(client, user)

            let sites = await client.getAll().prefix(`${sitesPrefix}:`).json()
            let peers = Object.entries(sites).map(([key, value]) => {
              let siteId = key.replace(`${sitesPrefix}:`, '')
              let allowed_ips = routesForSite(siteId, rules)
              return {
                public_key: value.public_key,
                endpoint: value.endpoint,
                allowed_ips,
                persistent_keepalive_interval: 25
              }
            })

            let responseConfig = {
              address: user.address,
              peers
            }

            let peerConfig = {
              public_key,
              allowed_ips: [{ ip: user.address, cidr: 32 }],
              persistent_keepalive_interval: 25,
              rules
            }
            console.log(`Adding peer`, peerConfig)
            await replacePeerTxn(response.email, peerConfig)
            return res.json(responseConfig)
          } else {
            console.log(`User ${response.email} not whitelisted`)
            return res.status(401).json({ error: 'User not whitelisted' })
          }
        }
      } catch (err) {
        if (err.statusCode === 401) {
          return res.status(401).json({ error: '401 Unauthorized' })
        } else {
          console.log(err)
          return res.status(500).json({ error: 'Internal Error' })
        }
      }
    } else {
      return res.status(400).json({ error: 'Missing Bearer token' })
    }
  } else {
    return res.status(400).json({ error: 'No authorization provided' })
  }


  res.json({ success: true })
})

async function isAdmin (email) {
  let user = await client.get(`${usersPrefix}:${email}`).json()
  return user && user.roles && user.roles.indexOf('wg-admin') !== -1
}

async function adminSession (session) {
  if (session && session.email) {
    return isAdmin(session.email)
  } else {
    return false
  }
}

async function ensureAdmin (req) {
  if (await adminSession(req.session)) {
    return true
  } else {
    throw new ApiError(401, 'Unauthorized')
  }
}

app.get('/login', async (req, res) => {
  try {
    if (req.session && req.session.email) {
      if (await isAdmin(req.session.email)) {
        return res.redirect('/users')
      } else {
        throw new Error('User does not have role wg-admin')
      }
    } else {
      let url
      if (isGoogle) {
        url = await generateURLGoogle()
      } else {
        url = await generateURLGeneric()
      }
      return res.redirect(url)
    }
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.get('/redirect', async (req, res) => {
  try {
    let email

    if (isGoogle) {
      email = await requestEmailGoogle(req.query.code, req.query.state)
    } else {
      email = await requestEmailGeneric(req.query.code, req.query.state)
    }

    if (email) {
      console.log('Login try from', email)
      if (await isAdmin(email)) {
        req.session.email = email
        return res.redirect('/users')
      } else {
        throw new ApiError(401, 'User does not have role wg-admin')
      }
    } else {
      throw new ApiErro(401, 'Failed to identify user')
    }
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.get('/api/users', async (req, res) => {
  try {
    await ensureAdmin(req)
    let users = await client.getAll().prefix(`${usersPrefix}:`).json()
    users = Object.entries(users || []).map(([k, v]) => {
      let email = k.replace(`${usersPrefix}:`, '')
      return {
        email,
        address: v.address,
        roles: v.roles || [],
        rules: v.rules || []
      }
    })
    return res.json(users)
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.post('/api/users', async (req, res) => {
  try {
    await ensureAdmin(req)
    let user = req.body
    if (user) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbUser = await tx.get(`${usersPrefix}:${user.email}`).json()
        if (dbUser) {
          throw new ApiError(400, 'User already exists')
        } else {
          console.log('Creating', user)
          let rolesUsers = await Promise.all((user.roles || [])
            .map(async role => {
              let key = `${roleUsersPrefix}:${role}`
              return [key, await tx.get(key).json()]
            }))

          let rolesUsersUpdates = rolesUsers.map(([key, roleUsers]) => {
            if (Array.isArray(roleUsers)) {
              roleUsers = roleUsers.concat([user.email])
            } else {
              roleUsers = [user.email]
            }

            return tx.put(key).value(JSON.stringify(roleUsers))
          })

          await Promise.all([
            tx.put(`${usersPrefix}:${user.email}`).value(JSON.stringify({
              address: user.address,
              roles: user.roles,
              rules: user.rules
            })),
            ...rolesUsersUpdates
          ])
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

function difference (a, b) {
  return a.filter(i => !b.includes(i))
}

app.put('/api/users', async (req, res) => {
  try {
    await ensureAdmin(req)
    let user = req.body
    if (user) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbUser = await tx.get(`${usersPrefix}:${user.email}`).json()
        if (dbUser) {
          console.log('Updating', user)

          // Updating roles
          let oldRoles = dbUser.roles || []
          let newRoles = user.roles || []

          let rolesToAdd = difference(newRoles, oldRoles)
          let rolesToDel = difference(oldRoles, newRoles)

          let rolesUsersToAdd = await Promise.all(rolesToAdd
            .map(async role => {
              let key = `${roleUsersPrefix}:${role}`
              return [key, await tx.get(key).json()]
            }))

          let rolesUsersAddOps = rolesUsersToAdd.map(([key, roleUsers]) => {
            if (Array.isArray(roleUsers)) {
              roleUsers = roleUsers.concat([user.email])
            } else {
              roleUsers = [user.email]
            }

            return tx.put(key).value(JSON.stringify(roleUsers))
          })

          let rolesUsersToDel = await Promise.all(rolesToDel
            .map(async role => {
              let key = `${roleUsersPrefix}:${role}`
              return [key, await tx.get(key).json()]
            }))

          let rolesUsersDelOps = rolesUsersToDel.map(([key, roleUsers]) => {
            if (Array.isArray(roleUsers)) {
              let index = roleUsers.indexOf(user.email)
              roleUsers.splice(index, 1)
            } else {
              roleUsers = []
            }

            return tx.put(key).value(JSON.stringify(roleUsers))
          })

          // Updating peer
          let peerUpdates = []
          let pubKey = await tx.get(`${userPublicKeyPrefix}:${user.email}`).json()
          if (pubKey) {
            let peer = await tx.get(`${peersPrefix}:${pubKey}`).json()
            if (peer.allowed_ips[0]) {
              peer.allowed_ips[0].ip = user.address
            }
            peer.rules = await resolveUserRules(tx, user)

            peerUpdates.push(tx.put(`${peersPrefix}:${pubKey}`).value(JSON.stringify(peer)))
          }

          // Commiting all
          await Promise.all([
            ...rolesUsersAddOps,
            ...rolesUsersDelOps,
            ...peerUpdates,
            tx.put(`${usersPrefix}:${user.email}`).value(JSON.stringify({
              address: user.address,
              roles: user.roles,
              rules: user.rules
            }))
          ])
        } else {
          throw new ApiError(404, 'User does not exists')
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.delete('/api/users', async (req, res) => {
  try {
    await ensureAdmin(req)
    let user = req.body
    if (user) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbUser = await tx.get(`${usersPrefix}:${user.email}`).json()
        if (dbUser) {
          console.log('Deleting', user)

          // Updating roles
          let rolesToDel = user.roles || []

          let rolesUsersToDel = await Promise.all(rolesToDel
            .map(async role => {
              let key = `${roleUsersPrefix}:${role}`
              return [key, await tx.get(key).json()]
            }))

          let rolesUsersDelOps = rolesUsersToDel.map(([key, roleUsers]) => {
            if (Array.isArray(roleUsers)) {
              let index = roleUsers.indexOf(user.email)
              roleUsers.splice(index, 1)
            } else {
              roleUsers = []
            }

            return tx.put(key).value(JSON.stringify(roleUsers))
          })

          // Updating peer
          let peerUpdates = []
          let pubKey = await tx.get(`${userPublicKeyPrefix}:${user.email}`).json()
          if (pubKey) {
            peerUpdates.push(
              tx.delete().key(`${userPublicKeyPrefix}:${user.email}`),
              tx.delete().key(`${peersPrefix}:${pubKey}`)
            )
          }

          // Commiting all
          await Promise.all([
            ...rolesUsersDelOps,
            ...peerUpdates,
            tx.delete().key(`${usersPrefix}:${user.email}`)
          ])
        } else {
          throw new ApiError(404, 'User does not exists')
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }

    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})


app.get('/api/roles', async (req, res) => {
  try {
    await ensureAdmin(req)
    let roles = await client.getAll().prefix(`${rolesPrefix}:`).json()
    roles = Object.entries(roles || []).map(([k, v]) => {
      let name = k.replace(`${rolesPrefix}:`, '')
      return {
        name,
        rules: v.rules || []
      }
    })
    return res.json(roles)
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.post('/api/roles', async (req, res) => {
  try {
    await ensureAdmin(req)
    let role = req.body
    if (role) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbRole = await tx.get(`${rolesPrefix}:${role.name}`).json()
        if (dbRole) {
          throw new ApiError(400, 'Role already exists')
        } else {
          console.log('Creating', role)

          await Promise.all([
            tx.put(`${rolesPrefix}:${role.name}`).value(JSON.stringify({
              rules: role.rules
            })),
            tx.put(`${roleUsersPrefix}:${role.name}`).value(JSON.stringify([]))
          ])
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.put('/api/roles', async (req, res) => {
  try {
    await ensureAdmin(req)
    let role = req.body
    if (role) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbRole = await tx.get(`${rolesPrefix}:${role.name}`).json()
        if (dbRole) {
          console.log('Updating', role)

          let roleUsers = await tx.get(`${roleUsersPrefix}:${role.name}`).json()

          let users = await Promise.all(roleUsers.map(async email => {
            return [email, await tx.get(`${usersPrefix}:${email}`).json()]
          }))


          let peers = await Promise.all(users.map(async ([email, user]) => {
            if (user) {
              let pubKey = await tx.get(`${userPublicKeyPrefix}:${email}`).json()

              if (pubKey) {
                let peer = await tx.get(`${peersPrefix}:${pubKey}`).json()
                if (peer) {
                  let rules = []
                  for (var r of user.roles || []) {
                    if (r.name === role.name) {
                      rules = rules.concat(role.rules || [])
                    } else {
                      let otherRole = await tx.get(`${rolesPrefix}:${r.name}`).json()
                      if (otherRole) {
                        rules = rules.concat(otherRole.rules || [])
                      }
                    }
                  }
                  rules = rules.concat(user.rules || [])

                  peer.rules = rules

                  return peer
                }
              }
            }

            return null
          }))

          let peerUpdates = peers.filter(p => !!p).map(peer => {
            return tx.put(`${peersPrefix}:${peer.public_key}`)
              .value(JSON.stringify(peer))
          })


          // Commiting all
          await Promise.all([
            ...peerUpdates,
            tx.put(`${rolesPrefix}:${role.name}`).value(JSON.stringify({
              rules: role.rules
            }))
          ])
        } else {
          throw new ApiError(404, 'Role does not exists')
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.delete('/api/roles', async (req, res) => {
  try {
    await ensureAdmin(req)
    let role = req.body
    if (role) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbRole = await tx.get(`${rolesPrefix}:${role.name}`).json()
        if (dbRole) {
          console.log('Deleting', role)

          let roleUsers = await tx.get(`${roleUsersPrefix}:${role.name}`).json()

          let users = await Promise.all(roleUsers.map(async email => {
            return [email, await tx.get(`${usersPrefix}:${email}`).json()]
          }))

          let userUpdates = users.map(([email, user]) => {
            if (user) {
              let index = user.roles.indexOf(role.name)
              user.roles.splice(index, 1)
              return tx.put(`${usersPrefix}:${email}`)
                .value(JSON.stringify(user))
            }
          })

          let peers = await Promise.all(users.map(async ([email, user]) => {
            if (user) {
              let pubKey = await tx.get(`${userPublicKeyPrefix}:${email}`).json()

              if (pubKey) {
                let peer = await tx.get(`${peersPrefix}:${pubKey}`).json()
                if (peer) {
                  let rules = []
                  for (var r of user.roles || []) {
                    if (r.name !== role.name) {
                      let otherRole = await tx.get(`${rolesPrefix}:${r.name}`).json()
                      if (otherRole) {
                        rules = rules.concat(otherRole.rules || [])
                      }
                    }
                  }
                  rules = rules.concat(user.rules || [])

                  peer.rules = rules

                  return peer
                }
              }
            }

            return null
          }))

          let peerUpdates = peers.filter(p => !!p).map(peer => {
            return tx.put(`${peersPrefix}:${peer.public_key}`)
              .value(JSON.stringify(peer))
          })

          // Commiting all
          await Promise.all([
            ...userUpdates,
            ...peerUpdates,
            tx.delete().key(`${roleUsersPrefix}:${role.name}`),
            tx.delete().key(`${rolesPrefix}:${role.name}`)
          ])
        } else {
          throw new ApiError(404, 'Role does not exists')
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

// TODO post, put, delete
app.get('/api/sites', async (req, res) => {
  try {
    await ensureAdmin(req)
    let sites = await client.getAll().prefix(`${sitesPrefix}:`).json()
    sites = Object.entries(sites || []).map(([k, v]) => {
      let id = k.replace(`${sitesPrefix}:`, '')
      return {
        id,
        address: v.address,
        listen_port: v.listen_port,
        endpoint: v.endpoint,
        public_key: v.public_key,
        private_key: v.private_key
      }
    })
    return res.json(sites)
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.post('/api/sites', async (req, res) => {
  try {
    await ensureAdmin(req)
    let site = req.body
    if (site) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbSite = await tx.get(`${sitesPrefix}:${site.id}`).json()
        if (dbSite) {
          throw new ApiError(400, 'Site already exists')
        } else {
          console.log('Creating', site)

          await Promise.all([
            tx.put(`${sitesPrefix}:${site.id}`).value(JSON.stringify({
              address: site.address,
              listen_port: site.listen_port,
              endpoint: site.endpoint,
              public_key: site.public_key,
              private_key: site.private_key
            }))
          ])
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.put('/api/sites', async (req, res) => {
  try {
    await ensureAdmin(req)
    let site = req.body
    if (site) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbSite = await tx.get(`${sitesPrefix}:${site.id}`).json()
        if (dbSite) {
          console.log('Updating', site)

          await Promise.all([
            tx.put(`${sitesPrefix}:${site.id}`).value(JSON.stringify({
              address: site.address,
              listen_port: site.listen_port,
              endpoint: site.endpoint,
              public_key: site.public_key,
              private_key: site.private_key
            }))
          ])
        } else {
          throw new ApiError(404, 'Site does not exists')
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.delete('/api/sites', async (req, res) => {
  try {
    await ensureAdmin(req)
    let site = req.body
    if (site) {
      await client.stm({ isolation: 2 }).transact(async (tx) => {
        let dbSite = await tx.get(`${sitesPrefix}:${site.id}`).json()
        if (dbSite) {
          console.log('Deleting', site)

          await Promise.all([
            tx.delete().key(`${sitesPrefix}:${site.id}`)
          ])
        } else {
          throw new ApiError(404, 'Site does not exists')
        }
      })
    } else {
      throw new ApiError(400, 'Missing body')
    }
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

app.get('/api/list', async (req, res) => {
  try {
    await ensureAdmin(req)
    let all = await client.getAll().prefix(``).json()
    console.log('-------------- START DUMP')
    console.log(all)
    console.log('-------------- END DUMP')
    return res.json({ success: true })
  } catch (err) {
    console.log(err)
    if (err instanceof ApiError) {
      return res.status(err.code).json({ code: err.code, message: err.message })
    } else {
      return res.status(500).json({ code: 500, message: err.message })
    }
  }
})

const startServer = async () => {
  const port = process.env.SERVER_PORT || 3000
  await promisify(app.listen).bind(app)(port)
  console.log(`Listening on port ${port}`)
}

async function main () {
  await startServer()
}

if (!module.parent) {
  main()
  .catch(err => {
    client.close()
    console.error(err)
    process.exit(1)
  })
  .then(console.log)
}

export {}
