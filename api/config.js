export const ETCD_CLIENT_CERT_FILE = process.env.ETCD_CLIENT_CERT_FILE
export const ETCD_CLIENT_KEY_FILE = process.env.ETCD_CLIENT_KEY_FILE
export const ETCD_CA_CERT_FILE = process.env.ETCD_CA_CERT_FILE

let sessionSecret = process.env.SESSION_SECRET

if (!sessionSecret) {
  console.log('[WARN] SESSION_SECRET not set, using weak development secret, this should not be done in production')
  sessionSecret = 'development-secret'
}

export const SESSION_SECRET = sessionSecret

export const OAUTH2_PROVIDER = process.env.OAUTH2_PROVIDER || 'google'

export const isGoogle = OAUTH2_PROVIDER === 'google'
export const isGeneric = OAUTH2_PROVIDER === 'generic'

if (!isGoogle && !isGeneric) {
  throw new Error(`There must be an OAUTH2_PROVIDER`)
}

export const CLIENT_ID = process.env.CLIENT_ID
export const CLIENT_SECRET = process.env.CLIENT_SECRET
export const REDIRECT_URI = process.env.REDIRECT_URI

if (!(CLIENT_ID && CLIENT_SECRET && REDIRECT_URI)) {
  throw new Error(`Missing OAUTH2 config (CLIENT_ID, CLIENT_SECRET, REDIRECT_URI)`)
}

export const AUTH_URL = process.env.AUTH_URL
export const TOKEN_URL = process.env.TOKEN_URL
export const SCOPE = process.env.SCOPE || (isGoogle && 'https://www.googleapis.com/auth/userinfo.email')

if (isGeneric && !(AUTH_URL && TOKEN_URL && SCOPE)) {
  throw new Error('Missing generic provider config (AUTH_URL, TOKEN_URL, SCOPE)')
}

export const PROFILE_URL = process.env.PROFILE_URL || (isGoogle && 'https://openidconnect.googleapis.com/v1/userinfo')

if (!PROFILE_URL) {
  throw new Error(`Missing PROFILE_URL`)
}

export const CONNECT_PROFILE_URL = process.env.CONNECT_PROFILE_URL || 'https://openidconnect.googleapis.com/v1/userinfo'
