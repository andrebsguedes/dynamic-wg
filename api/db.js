import { Etcd3 } from 'etcd3'
import fs from 'fs'

import {
  ETCD_CLIENT_CERT_FILE,
  ETCD_CLIENT_KEY_FILE,
  ETCD_CA_CERT_FILE
} from './config'

export const client = new Etcd3({
  hosts: [process.env.ETCD_HOST || `127.0.0.1:2379`],
  auth: process.env.ETCD_USER && {
    username: process.env.ETCD_USER,
    password: process.env.ETCD_PASS
  },
  credentials: ETCD_CA_CERT_FILE && {
    certChain: ETCD_CLIENT_CERT_FILE && fs.readFileSync(ETCD_CLIENT_CERT_FILE),
    privateKey: ETCD_CLIENT_KEY_FILE && fs.readFileSync(ETCD_CLIENT_KEY_FILE),
    rootCertificate: ETCD_CA_CERT_FILE && fs.readFileSync(ETCD_CA_CERT_FILE)
  }
})
