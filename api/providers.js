import { OAuth2Client } from 'google-auth-library'
import NodeCache from 'node-cache'
import got from 'got'
import crypto from 'crypto'
import {
  isGoogle,
  isGeneric,
  OAUTH2_PROVIDER,
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI,
  SCOPE,
  AUTH_URL,
  TOKEN_URL,
  PROFILE_URL,
  CONNECT_PROFILE_URL
} from './config'

export async function getConnectProfile (token) {
  const profile = await got.get(CONNECT_PROFILE_URL, {
    headers: {
      authorization: `Bearer ${token}`
    }
  }).json()

  return profile
}

export async function generateURLGoogle () {
  const authClient = new OAuth2Client(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI
  )

  const authorizeUrl = authClient.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPE
  })

  return authorizeUrl
}

export async function requestEmailGoogle (code, state) {
  const authClient = new OAuth2Client(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI
  )

  const response = await authClient.getToken(code)

  authClient.setCredentials(response.tokens)

  // console.log(req)
  const userResponse = await authClient.request({ url: PROFILE_URL })

  // console.log(userResponse)
  const email = userResponse && userResponse.data && userResponse.data.email
  return email
}

const stateCache = new NodeCache()

export async function generateURLGeneric () {
  let state = crypto.randomBytes(24).toString('hex')
  stateCache.set(state, true, 60)
  return `${AUTH_URL}?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=code&state=${state}&scope=${SCOPE}`
}

export async function requestEmailGeneric (code, state) {
  if (stateCache.get(state)) {
    const response = await got.post(TOKEN_URL, {
      searchParams: {
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        code,
        grant_type: 'authorization_code',
        redirect_uri: REDIRECT_URI
      }
      // responseType: 'text',
      // resolveBodyOnly: false,
      // throwHttpErrors: false
    }).json()

    let token = response.access_token || response.accessToken

    const profile = await got.get(PROFILE_URL, {
      headers: {
        authorization: `Bearer ${token}`
      }
    }).json()

    return profile.email
  }
}
