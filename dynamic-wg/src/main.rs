use tokio::net::UnixStream;
use tokio::prelude::*;
use tokio_net::process::Command;
use hex;
use boringtun::crypto::x25519::*;
// use std::os::unix::net::UnixStream;
// use std::io::Write;
// use std::io::Read;
use std::error::Error;
use std::net::AddrParseError;
use std::num::ParseIntError;
use std::io;
use std::str::FromStr;
use std::net::{IpAddr, SocketAddr, Ipv4Addr};
use serde_json;
use serde::{Deserialize, Serialize};
use futures_util::compat::Future01CompatExt;
use cidr::IpInet;

type Exception = Box<dyn Error + Send + Sync + 'static>;

#[derive(Debug, Serialize, Deserialize)]
struct AllowedIp {
    ip: IpAddr,
    cidr: u8
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum AllowedIpParseError {
    AddrError(AddrParseError),
    CidrError(ParseIntError),
    MissingError
}

impl std::fmt::Display for AllowedIpParseError {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fmt.write_str(self.description())
    }
}

impl Error for AllowedIpParseError {
    fn description(&self) -> &str {
        "invalid AllowedIp address syntax"
    }
}

impl From<AddrParseError> for AllowedIpParseError {
    fn from(error: AddrParseError) -> Self {
        AllowedIpParseError::AddrError(error)
    }
}

impl From<ParseIntError> for AllowedIpParseError {
    fn from(error: ParseIntError) -> Self {
        AllowedIpParseError::CidrError(error)
    }
}

impl FromStr for AllowedIp {
    type Err = AllowedIpParseError;
    fn from_str(s: &str) -> Result<AllowedIp, Self::Err> {
        let parts: Vec<_> = s.split('/').collect();

        if parts.len() == 2 {
            let ip = parts[0].parse()?;
            let cidr = parts[1].parse()?;
            Ok(AllowedIp { ip, cidr })
        } else {
            Err(AllowedIpParseError::MissingError)
        }
    }
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
enum RuleDirection {
    INGRESS,
    EGRESS
}

fn default_rule_direction() -> RuleDirection {
    RuleDirection::INGRESS
}

#[derive(Debug, Serialize, Deserialize)]
enum RuleProtocol {
    UDP,
    TCP,
    ICMP,
    ALL
}

fn default_rule_protocol() -> RuleProtocol {
    RuleProtocol::ALL
}

#[derive(Debug, Serialize, Deserialize)]
enum RuleAction {
    ALLOW,
    DENY
}

fn default_rule_action() -> RuleAction {
    RuleAction::ALLOW
}

#[derive(Debug, Serialize, Deserialize)]
enum RulePorts {
    ALL,
    Ports(Vec<u16>)
}

fn default_rule_ports() -> RulePorts {
    RulePorts::ALL
}

#[derive(Debug, Serialize, Deserialize)]
struct Rule {
    #[serde(default="default_rule_direction")]
    direction: RuleDirection,
    #[serde(default="default_rule_protocol")]
    protocol: RuleProtocol,
    #[serde(default="default_rule_action")]
    action: RuleAction,
    targets: Vec<IpInet>,
    #[serde(default="default_rule_ports")]
    ports: RulePorts,
    only: Option<Vec<String>>,
    except: Option<Vec<String>>,
}

fn default_rules() -> Vec<Rule> {
    vec!()
}

#[derive(Debug, Serialize, Deserialize)]
struct Peer {
    #[serde(with = "public_key_serde")]
    public_key: X25519PublicKey,
    endpoint: Option<SocketAddr>,
    allowed_ips: Vec<AllowedIp>,
    persistent_keepalive_interval: Option<u16>,
    last_handshake_time_sec: Option<usize>,
    last_handshake_time_nsec: Option<usize>,
    rx_bytes: Option<usize>,
    tx_bytes: Option<usize>,
    #[serde(default="default_rules")]
    rules: Vec<Rule>
}

impl Default for Peer {
    fn default() -> Self {
        let private = X25519SecretKey::new();
        Peer {
            public_key: private.public_key(),
            endpoint: None,
            allowed_ips: vec!(),
            persistent_keepalive_interval: None,
            last_handshake_time_sec: None,
            last_handshake_time_nsec: None,
            rx_bytes: None,
            tx_bytes: None,
            rules: vec!()
        }
    }
}

mod secret_key_serde {
    use boringtun::crypto::x25519::*;
    use std::str::FromStr;
    use hex;
    use serde::{self, Deserialize, Serializer, Deserializer};

    pub fn serialize<S>(
        private_key: &X25519SecretKey,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = hex::encode(private_key.as_bytes());
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(
        deserializer: D,
    ) -> Result<X25519SecretKey, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        X25519SecretKey::from_str(&s).map_err(serde::de::Error::custom)
    }
}

mod public_key_serde {
    use boringtun::crypto::x25519::*;
    use std::str::FromStr;
    use hex;
    use serde::{self, Deserialize, Serializer, Deserializer};

    pub fn serialize<S>(
        public_key: &X25519PublicKey,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = hex::encode(public_key.as_bytes());
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(
        deserializer: D,
    ) -> Result<X25519PublicKey, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        X25519PublicKey::from_str(&s).map_err(serde::de::Error::custom)
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Interface {
    #[serde(default="default_address")]
    address: IpInet,
    endpoint: Option<SocketAddr>,
    #[serde(default="default_listen_port")]
    listen_port: u16,
    #[serde(with = "secret_key_serde", default="default_secret_key")]
    private_key: X25519SecretKey,
    peers: Vec<Peer>
}

fn default_address () -> IpInet {
    "10.1.0.1/24".parse().unwrap()
}

fn default_secret_key () -> X25519SecretKey {
    X25519SecretKey::new()
}

fn default_listen_port () -> u16 {
    53050
}

impl Default for Interface {
    fn default() -> Self {
        Interface {
            address: default_address(),
            endpoint: None,
            listen_port: default_listen_port(),
            private_key: default_secret_key(),
            peers: vec!()
        }
    }
}


#[derive(Debug, Serialize, Deserialize)]
struct InterfaceDetails {
    listen_port: u16,
    #[serde(with = "secret_key_serde")]
    private_key: X25519SecretKey,
    #[serde(with = "public_key_serde")]
    public_key: X25519PublicKey,
    peers: Vec<Peer>
}

impl Default for InterfaceDetails {
    fn default() -> Self {
        let private = X25519SecretKey::new();
        InterfaceDetails {
            listen_port: u16::default(),
            public_key: private.public_key(),
            private_key: private,
            peers: vec!()
        }
    }
}

fn parse_response_pairs<'a> (response: &'a str) -> Vec<(&'a str, &'a str)> {
    if response.len() == 0 {
        return vec!();
    }

    let trimmed = response.trim_end();
    let splited: Vec<_> = trimmed.split("\n").map(|p| {
        let pair: Vec<_> = p.split("=").collect();
        (pair[0], pair[1])
    }).collect();
    splited
}

async fn get(name: &str) -> Result<String, Exception> {
    let path = format!("/var/run/wireguard/{}.sock", name);
    let mut socket = UnixStream::connect(path).await?;
    socket.write_all(b"get=1\n\n").await?;
    let mut ret = String::new();
    socket.read_to_string(&mut ret).await?;
    Ok(ret)
}

async fn get_interface(name: &str) -> Result<InterfaceDetails, Exception> {
    let response = get(name).await?;

    let mut details: InterfaceDetails = InterfaceDetails::default();

    let splited = parse_response_pairs(response.as_str());
    println!("{:?}", splited);
    for (key, value) in splited.iter() {
        match *key {
            "listen_port" => {
                details.listen_port = value.parse()?;
            }
            "private_key" => {
                details.private_key = X25519SecretKey::from_str(value)?;
                details.public_key = details.private_key.public_key();
            }
            "errno" => {
                if *value != "0" {
                    return Err(io::Error::new(io::ErrorKind::Other, format!("Failed with code {}", value)).into())
                }
            }
            _ => ()
        }
    }

    details.peers = peers_parser(splited)?;
    Ok(details)
}

async fn get_peer(name: &str, peer: &Peer) -> Result<Option<Peer>, Exception> {
    let payload = serialize_peer(peer);
    let response = get(name).await?;
    let pairs = parse_response_pairs(response.as_str());
    for (key, value) in pairs.iter() {
        match *key {
            "errno" => {
                if *value != "0" {
                    return Err(io::Error::new(io::ErrorKind::Other, format!("Failed with code {}", value)).into())
                }
            }
            _ => ()
        }
    }
    let mut peers = peers_parser(pairs)?;
    let out = peers.into_iter().find(|p| p.public_key.as_bytes() == peer.public_key.as_bytes());
    Ok(out)
}

async fn set(name: &str, key: &str, value: &str) -> Result<(), Exception> {
    let payload_string = format!("{}={}", key, value);
    set_payload(name, payload_string.as_str()).await
}

async fn set_payload(name: &str, payload: &str) -> Result<(), Exception> {
    let path = format!("/var/run/wireguard/{}.sock", name);
    let mut socket = UnixStream::connect(path).await?;


    let string = format!("set=1\n{}\n\n", payload);
    let bytes = string.as_bytes();
    socket.write_all(bytes).await?;

    let mut ret = String::new();
    socket.read_to_string(&mut ret).await?;

    for (key, value) in parse_response_pairs(ret.as_str()) {
        match (key, value) {
            ("errno", v) if v != "0" => {
                return Err(io::Error::new(io::ErrorKind::Other, format!("Failed with code {}", value)).into())
            },
            _ => ()
        }
    }
    Ok(())
}

#[derive(Debug, Clone, Copy)]
enum State {
    InPeer,
    OutPeer,
    End
}

fn peers_parser(pairs: Vec<(&str, &str)>) -> Result<Vec<Peer>, Exception> {
    use State::*;

    let mut peers = vec!();
    let mut peer = Peer::default();

    let mut state = OutPeer;
    for (key, value) in pairs {
        match (state, key) {
            (OutPeer, "public_key") => {
                peer.public_key = X25519PublicKey::from_str(value)?;
                state = InPeer;
            }
            (InPeer, k) => {
                match k {
                    "endpoint" => {
                        peer.endpoint = Some(value.parse()?);
                    }
                    "persistent_keepalive_interval" => {
                        peer.persistent_keepalive_interval = Some(value.parse()?);
                    }
                    "allowed_ip" => {
                        peer.allowed_ips.push(value.parse()?);
                    }
                    "last_handshake_time_sec" => {
                        peer.last_handshake_time_sec = Some(value.parse()?);
                    }
                    "last_handshake_time_nsec" => {
                        peer.last_handshake_time_nsec = Some(value.parse()?);
                    }
                    "rx_bytes" => {
                        peer.rx_bytes = Some(value.parse()?);
                    }
                    "tx_bytes" => {
                        peer.tx_bytes = Some(value.parse()?);
                        peers.push(peer);
                        peer = Peer::default();
                        state = OutPeer;
                    }
                    _ => {
                        unimplemented!("Field not implemented");
                    }
                }
            }
            _ => {}
        }
    }

    Ok(peers)
}

fn serialize_peer(peer: &Peer) -> String {
    let mut tuples = vec![("public_key", hex::encode(peer.public_key.as_bytes()))];
    if let Some(endpoint) = peer.endpoint {
        tuples.push(("endpoint", format!("{}", endpoint)));
    }

    for AllowedIp { ip, cidr } in peer.allowed_ips.iter() {
        tuples.push(("allowed_ip", format!("{}/{}", ip, cidr)));
    }

    if let Some(keepalive) = peer.persistent_keepalive_interval {
        tuples.push(("persistent_keepalive_interval", format!("{}", keepalive)));
    }

    let strings: Vec<_> = tuples.iter().map(|(k, v)| format!("{}={}", k, v)).collect();
    strings.join("\n")
}

async fn update_peer(name: &str, peer: &Peer) -> Result<(), Exception> {
    del_peer(name, peer).await?;
    add_peer(name, peer).await?;
    Ok(())
}

async fn add_peer(name: &str, peer: &Peer) -> Result<(), Exception> {
    let payload = serialize_peer(peer);
    set_payload(name, payload.as_str()).await?;
    setup_peer_rules(name, peer).await?;
    Ok(())
}

async fn del_peer(name: &str, peer: &Peer) -> Result<(), Exception> {
    let payload = serialize_peer(peer);
    let del_string = format!("{}\nremove=true", payload);
    set_payload(name, del_string.as_str()).await?;
    teardown_peer_rules(name, peer).await?;
    Ok(())
}

async fn ensure_command(mut command: tokio_net::process::Command) -> Result<std::process::Output, Exception> {
    let output = command.output().await?;
    if output.status.success() {
        return Ok(output)
    } else {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("Command {:?} failed with code {:?} and stderr {:?}", command, output.status.code(), String::from_utf8_lossy(output.stderr.as_ref()))
        ).into())
    }
}

async fn setup_peer_rules(iface: &str, peer: &Peer) -> Result<(), Exception> {
    let peer_ip_opt = peer.allowed_ips.first();

    if let Some(AllowedIp { ip, cidr: 32 }) = peer_ip_opt {
        let ip_string = ip.to_string();
        let chain_string = format!("DYNWG-PEER-{}", ip_string.replace(".", "-"));
        let chain = chain_string.as_str();
        let source_string = format!("{}/32", ip_string);
        let source = source_string.as_str();

        let _ensure_peer_chain = Command::new("iptables-legacy")
            .args(&["-N", chain]).output().await?;

        let _flush_chain = Command::new("iptables-legacy")
            .args(&["-F", chain]).output().await?;

        for rule in peer.rules.iter() {
            let mut command = Command::new("iptables-legacy");
            command.args(&["-A", chain]);

            if rule.direction == RuleDirection::INGRESS {
                command.args(&["-i", iface]);
                command.args(&["--source", source]);
                let target_strings: Vec<_> = rule.targets.iter().map(|t| t.to_string()).collect();
                let targets_string = target_strings.join(",");
                command.args(&["--destination", targets_string.as_str()]);
            } else {
                command.args(&["-o", iface]);
                command.args(&["--destination", source]);
                let target_strings: Vec<_> = rule.targets.iter().map(|t| t.to_string()).collect();
                let targets_string = target_strings.join(",");
                command.args(&["--source", targets_string.as_str()]);
            }

            match rule.protocol {
                RuleProtocol::TCP => {
                    command.args(&["-p", "tcp"]);
                }
                RuleProtocol::UDP => {
                    command.args(&["-p", "udp"]);
                }
                RuleProtocol::ICMP => {
                    command.args(&["-p", "icmp"]);
                }
                RuleProtocol::ALL => {
                    command.args(&["-p", "all"]);
                }
            }

            match rule.ports {
                RulePorts::ALL => {}
                RulePorts::Ports(ref ports) => {
                    for port in ports.iter() {
                        command.args(&["--dport", port.to_string().as_str()]);
                    }
                }
            }

            match rule.action {
                RuleAction::ALLOW => {
                    command.args(&["-j", "ACCEPT"]);
                }
                RuleAction::DENY => {
                    command.args(&["-j", "DROP"]);
                }
            }


            ensure_command(command).await?;
        }

        let mut append_return = Command::new("iptables-legacy");
        append_return.args(&["-A", chain, "-j", "RETURN"]);

        ensure_command(append_return).await?;

        let mut delete_ingress_jump = Command::new("iptables-legacy");
        delete_ingress_jump.args(&["-D", "FORWARD", "-i", iface, "-s", source, "-j", chain]);
        delete_ingress_jump.output().await?;

        let mut append_ingress_jump = Command::new("iptables-legacy");
        append_ingress_jump.args(&["-A", "FORWARD", "-i", iface, "-s", source, "-j", chain]);
        append_ingress_jump.output().await?;

        let mut delete_conntrack_accept = Command::new("iptables-legacy");
        delete_conntrack_accept.args(&["-D", "FORWARD", "-o", iface, "-d", source, "-m", "conntrack", "--ctstate", "RELATED,ESTABLISHED", "-j", "ACCEPT"]);
        delete_conntrack_accept.output().await?;

        let mut append_conntrack_accept = Command::new("iptables-legacy");
        append_conntrack_accept.args(&["-A", "FORWARD", "-o", iface, "-d", source, "-m", "conntrack", "--ctstate", "RELATED,ESTABLISHED", "-j", "ACCEPT"]);
        append_conntrack_accept.output().await?;

        let mut delete_egress_jump = Command::new("iptables-legacy");
        delete_egress_jump.args(&["-D", "FORWARD", "-o", iface, "-d", source, "-j", chain]);
        delete_egress_jump.output().await?;

        let mut append_egress_jump = Command::new("iptables-legacy");
        append_egress_jump.args(&["-A", "FORWARD", "-o", iface, "-d", source, "-j", chain]);
        append_egress_jump.output().await?;
    }

    Ok(())
}

async fn teardown_peer_rules(iface: &str, peer: &Peer) -> Result<(), Exception> {
    let peer_ip_opt = peer.allowed_ips.first();

    if let Some(AllowedIp { ip, cidr: 32 }) = peer_ip_opt {
        let ip_string = ip.to_string();
        let chain_string = format!("DYNWG-PEER-{}", ip_string.replace(".", "-"));
        let chain = chain_string.as_str();
        let source_string = format!("{}/32", ip_string);
        let source = source_string.as_str();

        let mut delete_ingress_jump = Command::new("iptables-legacy");
        delete_ingress_jump.args(&["-D", "FORWARD", "-i", iface, "-s", source, "-j", chain]);
        delete_ingress_jump.output().await?;

        let mut delete_egress_jump = Command::new("iptables-legacy");
        delete_egress_jump.args(&["-D", "FORWARD", "-o", iface, "-d", source, "-j", chain]);
        delete_egress_jump.output().await?;

        let mut delete_conntrack_accept = Command::new("iptables-legacy");
        delete_conntrack_accept.args(&["-D", "FORWARD", "-o", iface, "-d", source, "-m", "conntrack", "--ctstate", "RELATED,ESTABLISHED", "-j", "ACCEPT"]);
        delete_conntrack_accept.output().await?;

        let _flush_chain = Command::new("iptables-legacy")
            .args(&["-F", chain]).output().await?;

        let _delete_peer_chain = Command::new("iptables-legacy")
            .args(&["-X", chain]).output().await?;
    } else {
        println!("Missing peer allowed ip, no rule teardown will be made");
    }

    Ok(())
}


use rand::{thread_rng, Rng};

async fn setup_interface(name: &str, interface: &Interface) -> Result<(), Exception> {
    let key_string = hex::encode(interface.private_key.as_bytes());
    set(name, "private_key", key_string.as_str()).await?;
    let port_string = format!("{}", interface.listen_port);
    set(name, "listen_port", port_string.as_str()).await?;

    for peer in interface.peers.iter() {
        add_peer(name, peer).await?;
    }

    let flush = Command::new("ip")
        .args(&["addr", "flush", "dev", name]).output();
    let out = flush.await?;
    assert!(out.status.success());

    let add_addr = Command::new("ip")
        .args(&["addr", "add", format!("{}", interface.address).as_str(), "dev", name]).output();
    let out = add_addr.await?;
    assert!(out.status.success());

    let iface_up = Command::new("ip")
        .args(&["link", "set", name, "up"]).output();
    let out = iface_up.await?;
    assert!(out.status.success());

    let no_nat = std::env::var("NO_NAT").is_ok();
    let no_forward_all = std::env::var("NO_FORWARD_ALL").is_ok();
    let forward_drop = std::env::var("FORWARD_DROP").is_ok();

    // TODO add rule

    if !no_forward_all {
        let iptables_forward = Command::new("iptables-legacy")
            .args(&["-A", "FORWARD", "-i", name, "-j", "ACCEPT"]).output();
        let out = iptables_forward.await?;
        assert!(out.status.success());
    }

    if forward_drop {
        let iptables_forward_drop = Command::new("iptables-legacy")
            .args(&["-P", "FORWARD", "DROP"]).output();
        let out = iptables_forward_drop.await?;
        assert!(out.status.success());
    }

    if !no_nat {
        let iptables_nat = Command::new("iptables-legacy")
            .args(&["-t", "nat", "-A", "POSTROUTING", "!", "-o", name, "-j", "MASQUERADE"]).output();
        let out = iptables_nat.await?;
        assert!(out.status.success());
    }

    let ip_forward = Command::new("sysctl")
        .args(&["-w", "net.ipv4.ip_forward=1"]).output();
    let out = ip_forward.await?;
    // assert!(out.status.success());

    Ok(())
}

async fn setup (name: &str) -> Result<(), Exception> {
    let secret_key = X25519SecretKey::new();
    set(name, "private_key", hex::encode(secret_key.as_bytes()).as_str()).await?;
    let mut rng = thread_rng();
    let add_addr = Command::new("ip")
        .args(&["addr", "add", format!("10.1.0.{}/24", rng.gen_range(0, 254)).as_str(), "dev", name]).output();
    let out = add_addr.await?;
    assert!(out.status.success());
    let iface_up = Command::new("ip")
        .args(&["link", "set", name, "up"]).output();
    let out = iface_up.await?;
    assert!(out.status.success());
    let list_all = Command::new("ip").arg("a").output();
    let out = list_all.await?;
    println!("{:#?}", out);
    Ok(())
}

use hyper::{Body, Error as HyperError, Request, Response, Server, Method, StatusCode};
use hyper::service::{make_service_fn, service_fn};
use futures_util::TryStreamExt;
use serde_json::json;

fn json_error_response(code: StatusCode, message: &str) -> Response<Body> {
    let body = json!({ "code": code.as_u16(), "error": message }).to_string();
    Response::builder()
        .status(code)
        .body(Body::from(body))
        .unwrap()
}

async fn router_service (req: Request<Body>) -> Result<Response<Body>, HyperError> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/interface") => {
            let details = get_interface("utun101").await.unwrap();
            let response = Response::builder()
                .header("Content-Type", "application/json")
                .status(StatusCode::OK)
                .body(Body::from(serde_json::to_string_pretty(&details).unwrap()))
                .unwrap();
            Ok(response)
        }
        (&Method::POST, "/interface") => {
            let entire_body = req.into_body().try_concat().await?;
            let string = String::from_utf8(entire_body.to_vec()).unwrap();
            let iface : Interface = match serde_json::from_str(&string) {
                Ok(i) => i,
                Err(e) => {
                    return Ok(json_error_response(StatusCode::BAD_REQUEST,
                                        format!("Failed to parse interface: {}", e).as_str()))
                }
            };

            setup_interface("utun101", &iface).await.unwrap();
            let details = get_interface("utun101").await.unwrap();

            let response = Response::builder()
                .header("Content-Type", "application/json")
                .status(StatusCode::OK)
                .body(Body::from(serde_json::to_string_pretty(&details).unwrap()))
                .unwrap();
            Ok(response)
        }
        (&Method::GET, "/peer") => {
            let entire_body = req.into_body().try_concat().await?;
            let string = String::from_utf8(entire_body.to_vec()).unwrap();
            let peer : Peer = match serde_json::from_str(&string) {
                Ok(p) => p,
                Err(e) => {
                    return Ok(json_error_response(StatusCode::BAD_REQUEST,
                                        format!("Failed to parse peer: {}", e).as_str()))
                }
            };

            let out = match get_peer("utun101", &peer).await {
                Ok(Some(p)) => p,
                Ok(None) => {
                    return Ok(json_error_response(StatusCode::NOT_FOUND, "Peer not found"))
                }
                Err(e) => {
                    return Ok(json_error_response(StatusCode::INTERNAL_SERVER_ERROR,
                                        format!("Failed to add peer on interface: {}", e).as_str()))
                }
            };

            let response = Response::builder()
                .header("Content-Type", "application/json")
                .status(StatusCode::OK)
                .body(Body::from(serde_json::to_string_pretty(&out).unwrap()))
                .unwrap();
            Ok(response)
        }
        (&Method::POST, "/peer") => {
            let entire_body = req.into_body().try_concat().await?;
            let string = String::from_utf8(entire_body.to_vec()).unwrap();
            let peer : Peer = match serde_json::from_str(&string) {
                Ok(p) => p,
                Err(e) => {
                    return Ok(json_error_response(StatusCode::BAD_REQUEST,
                                        format!("Failed to parse peer: {}", e).as_str()))
                }
            };

            if let Err(e) = add_peer("utun101", &peer).await {
                return Ok(json_error_response(StatusCode::INTERNAL_SERVER_ERROR,
                                              format!("Failed to add peer on interface: {}", e).as_str()))
            };

            let response = Response::builder()
                .header("Content-Type", "application/json")
                .status(StatusCode::OK)
                .body(Body::from(serde_json::to_string_pretty(&peer).unwrap()))
                .unwrap();
            Ok(response)
        }
        (&Method::PUT, "/peer") => {
            let entire_body = req.into_body().try_concat().await?;
            let string = String::from_utf8(entire_body.to_vec()).unwrap();
            let peer : Peer = match serde_json::from_str(&string) {
                Ok(p) => p,
                Err(e) => {
                    return Ok(json_error_response(StatusCode::BAD_REQUEST,
                                        format!("Failed to parse peer: {}", e).as_str()))
                }
            };

            if let Err(e) = update_peer("utun101", &peer).await {
                return Ok(json_error_response(StatusCode::INTERNAL_SERVER_ERROR,
                                              format!("Failed to update peer on interface: {}", e).as_str()))
            };

            let response = Response::builder()
                .header("Content-Type", "application/json")
                .status(StatusCode::OK)
                .body(Body::from(serde_json::to_string_pretty(&peer).unwrap()))
                .unwrap();
            Ok(response)
        }
        (&Method::DELETE, "/peer") => {
            let entire_body = req.into_body().try_concat().await?;
            let string = String::from_utf8(entire_body.to_vec()).unwrap();
            let peer : Peer = match serde_json::from_str(&string) {
                Ok(p) => p,
                Err(e) => {
                    return Ok(json_error_response(StatusCode::BAD_REQUEST,
                                        format!("Failed to parse peer: {}", e).as_str()))
                }
            };

            if let Err(e) = del_peer("utun101", &peer).await {
                return Ok(json_error_response(StatusCode::INTERNAL_SERVER_ERROR,
                                              format!("Failed to delete peer on interface: {}", e).as_str()))
            };

            let response = Response::builder()
                .header("Content-Type", "application/json")
                .status(StatusCode::OK)
                .body(Body::from(serde_json::to_string_pretty(&peer).unwrap()))
                .unwrap();
            Ok(response)
        }
        _ => {
            Ok(json_error_response(StatusCode::NOT_FOUND, "NOT_FOUND"))
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Exception> {
    let addr = ([0, 0, 0, 0], 3000).into();
    let make_service = make_service_fn(|_| async {
        Ok::<_, HyperError>(service_fn(router_service))
    });
    let server = Server::bind(&addr).serve(make_service);
    println!("Test");
    let iface = Interface::default();
    setup_interface("utun101", &iface).await?;
    println!("Setup");
    let details = get_interface("utun101").await?;
    println!("{:?}", details);
    server.await?;
    Ok(())
}
