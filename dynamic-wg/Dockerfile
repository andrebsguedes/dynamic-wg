# syntax = docker/dockerfile:1.0-experimental

FROM rust:1.37-slim AS build

RUN rustup install nightly
RUN rustup default nightly
RUN apt update -y && apt install -y pkg-config libssl-dev

WORKDIR /rust-src

COPY Cargo.toml /rust-src/Cargo.toml
COPY src /rust-src/src

RUN --mount=type=cache,target=/rust-src/target \
    --mount=type=cache,target=/usr/local/cargo/git \
    --mount=type=cache,target=/usr/local/cargo/registry \
    ["cargo", "build", "--release", "--bin", "dynamic-wg"]

RUN --mount=type=cache,target=/rust-src/target \
    ["cp", "/rust-src/target/release/dynamic-wg", "/usr/local/bin/dynamic-wg"]

FROM registry.gitlab.com/andrebsguedes/dynamic-wg/boringtun:latest

RUN apt update -y && apt install -y iproute2 iputils-ping curl iptables procps netcat

WORKDIR /app

COPY --from=build /usr/local/bin/dynamic-wg /app/dynamic-wg

ENV WG_LOG_LEVEL=debug

CMD ["/bin/bash", "-c", "set -ex; /app/boringtun --disable-drop-privileges true $INTERFACE_NAME && /app/dynamic-wg"]
